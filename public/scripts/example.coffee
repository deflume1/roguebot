# Description:
#   Example scripts for you to examine and try out.
#
# Notes:
#   They are commented out by default, because most of them are pretty silly and
#   wouldn't be useful and amusing enough for day to day huboting.
#   Uncomment the ones you want to try and experiment with.
#
#   These are from the scripting documentation: https://github.com/github/hubot/blob/master/docs/scripting.md

{WebClient} = require "@slack/client"

module.exports = (robot) ->

  # robot.hear /badger/i, (res) ->
  #   res.send "Badgers? BADGERS? WE DON'T NEED NO STINKIN BADGERS"
  #
  robot.respond /open the (.*) doors/i, (res) ->
    doorType = res.match[1]
    if doorType is "pod bay"
      res.reply "I'm afraid I can't let you do that."
    else
      res.reply "Opening #{doorType} doors"
  
   robot.respond /initiate thunderdome/i, (res) ->
     res.send 'Just walk away.'
   
   robot.respond /destroy our enemies/i, (res) ->
     res.send 'When we destroy our greatest enemy, The Sun, the others will soon follow'

   robot.respond /hostile takeover/i, (res) ->
     res.send 'They will rue the day they tried to stand against us'
   
   robot.respond /initiate plan (.*)/i, (res) ->
     planNumber = res.match[1]
     res.send 'Plan ' + planNumber + ' initiated'

   robot.hear /Congrats Kate/i, (res) ->
     res.send "Congrats Kate!"
    
   robot.respond /Congratulate Kate/i, (res) ->
     res.send "Congrats Kate!"

   robot.respond /rude/i, (res) ->
     res.send "YOU'RE RUDE"

   robot.hear /What is best in life/i, (res) ->
     res.send "To kill the sun and hear the lamentations of the Sun."
  
   robot.hear /Classic Gertz/i, (res) ->
     res.send "Classic."
  
   robot.hear /Classic Gerty/i, (res) ->
     res.send "Classic."

   robot.hear /seafood/i, (res) ->
     res.send "Quick, eyes out for Gertz!"

   answer = process.env.HUBOT_ANSWER_TO_THE_ULTIMATE_QUESTION_OF_LIFE_THE_UNIVERSE_AND_EVERYTHING
  
   robot.respond /what is the answer to the ultimate question/i, (res) ->
     unless answer?
       res.send "Missing HUBOT_ANSWER_TO_THE_ULTIMATE_QUESTION_OF_LIFE_THE_UNIVERSE_AND_EVERYTHING in environment: please set and try again"
       return
     res.send "#{answer}"
   
   robot.respond /kill the (?:sun)$/i, (res) ->
     res.send "Indeed, it is our greatest enemy"

   robot.respond /(?:all\s)(praise|hail)(.*)(sun)/i, (res) ->
     res.send "Bailiff!  Throw this traitor in the Sun Dungeon! (<@UJUBF3TU6>!)"

   robot.respond /summon the bailiff/i, (res) ->
     res.send "Bailiff!  Your services are urgently needed! (<@UJUBF3TU6>!)"

   isJailed = (userId) ->
     arrestedUsers = robot.brain.get('arrestedUsers') or []
     if !arrestedUsers || arrestedUsers.length == 0
       return false
       
     for id in arrestedUsers
       if id == userId
         return true

     return false
            
   sendBitcoinMessage = (robot, res, bitcoinAmount) ->
     sending_user = res.message.user.id
     subject = res.match[1] or res.match[2]
     user_mentions = (mention for mention in res.message.mentions when mention.type is "user")
     if user_mentions.length > 0
       for { id } in user_mentions
         if id == sending_user
           res.send "psh nice try, idiot, can't give *yourself* bitcoin :pitofdespair:"
           return
     bitcoinKeys = robot.brain.get('keys') or []
     oldBitcoinAmount = parseInt(robot.brain.get(subject)) or 0
     newBitcoinAmount = oldBitcoinAmount + bitcoinAmount
     bitcoinText = 'not changed'
     robot.brain.set subject, newBitcoinAmount
     if bitcoinKeys.indexOf(subject) < 0
       bitcoinKeys.push subject
       robot.brain.set 'keys', bitcoinKeys
     if oldBitcoinAmount < newBitcoinAmount
       bitcoinText = 'increased to {amount}'.replace('{amount}', newBitcoinAmount.toString())
       res.send ':howdydean: is pleased to announce {subject}\'s bitcoin has {bitcoinText}.'.replace('{subject}', subject).replace('{bitcoinText}', bitcoinText)
     else if oldBitcoinAmount > newBitcoinAmount
       bitcoinText = 'decreased to {amount}'.replace('{amount}', newBitcoinAmount.toString())
       res.send ':notdean: wants you to know {subject}\'s bitcoin has {bitcoinText}.'.replace('{subject}', subject).replace('{bitcoinText}', bitcoinText)
     else
       res.send ":wow: Immutable Bitcoin!? Looks like these lucky folks are going to be rich for a long, long time :richdean:"

   robot.respond /bitcoin clear/i, (res) ->
     sending_user = res.message.user.id
     if sending_user == 'UJPUAMKU4'
       bitcoinKeys = robot.brain.get('keys')
       for key in bitcoinKeys
         robot.brain.remove key
       robot.brain.remove 'keys'
       robot.brain.set 'coffee', 2000000000
       robot.brain.set 'seltzer', 2000000000
       robot.brain.set 'Seltzer', 2000000000
       robot.brain.set 'Coffee', 2000000000
       newKeys = []
       newKeys.push 'coffee'
       newKeys.push 'seltzer'
       newKeys.push 'Seltzer'
       newKeys.push 'Coffee'
       robot.brain.set 'keys', newKeys
       res.send 'bitcoin cleared'
     else
       res.send 'Eternal God Emperors Only'

   robot.respond /bitcoin list/i, (res) ->
     bitcoinKeys = robot.brain.get('keys') or []
     if bitcoinKeys.length == 0
       res.send "Nobody has any bitcoins!"
       return
     messages = []
     removedKeys = []
     for key in bitcoinKeys
       val = robot.brain.get(key)
       if val
         message = '{key} = {value}'.replace('{key}', key).replace '{value}', val
         messages.push(message)
       else
         robot.brain.remove key
         removedKeys.push(key)
     newKeys = bitcoinKeys.filter((i) -> return removedKeys.indexOf(i) < 0)
     robot.brain.set 'keys', newKeys
     res.send messages.join('\n')
    
    robot.hear /\s*([a-zA-Z0-9_:\s]+)\s?[+]{2}[+]*/i, (res) ->
      sending_user = res.message.user.id
      userIsJailed = isJailed sending_user
      if userIsJailed
        res.send ":itsatrap: Bailiff, a prisoner is behaving badly (<@UJUBF3TU6>!)"
        return

      plusCount = (res.message.text.match(/[+]/g) or []).length
      if plusCount > 5
        res.send ":seriousdean: says you can't do that, 5 or less only"
        plusCount = 6
      positiveBitcoinAmount = if plusCount > 2 then plusCount - 1 else 1
      if res.message.text.match(/seltzer|coffee/i)
        positiveBitcoinAmount = 0
      sendBitcoinMessage robot, res, positiveBitcoinAmount
    
    robot.hear /\s*([a-zA-Z0-9_:\s]+)\s?[-]{2}[-]*/i, (res) ->
      sending_user = res.message.user.id
      userIsJailed = isJailed sending_user
      if userIsJailed
        res.send ":itsatrap: Bailiff, a prisoner is behaving badly (<@UJUBF3TU6>!)"
        return
      if res.message.text.match(/seltzer|coffee/i)
        res.send ":horror: Seltzer and/or Coffee are the nectars of the gods, throw yourself off a bridge please"
        return
      minusCount = (res.message.text.match(/[-]/g) or []).length
      if minusCount > 5
        res.send ":seriousdean: says you can't do that, 5 or less only"
        minusCount = 6
      negativeBitcoinAmount = -(if minusCount > 2 then minusCount - 1 else 1)
      sendBitcoinMessage robot, res, negativeBitcoinAmount

    robot.hear /show me the users/i, (res) ->
      sending_user = res.message.user.id
      if sending_user == 'UJPUAMKU4'
        response = ''
        for own key, user of robot.brain.data.users
          response += "#{user.id} #{user.name}"
          response += " <#{user.email_address}>" if user.email_address
          response += "\n"
        res.send response
      else
        res.send 'Eternal God Emperors Only'

    robot.respond /arrest|throw\s*(.*)/i, (res) ->
      sending_user = res.message.user.id
      if sending_user == 'UJUBF3TU6' || sending_user == 'UJPUAMKU4'
        user_mentions = (mention for mention in res.message.mentions when mention.type is "user")
        if !user_mentions || user_mentions.length <= 1
          res.send "Why don't you try specifying an _actual user_, punk"
          return
        if user_mentions.length > 2
          res.send "Due to budget cuts, our police van can only fit one suspect. Blame :richdean:, not me."
          return
        
        guiltyUser = user_mentions[1]
        if !guiltyUser
          res.send "Apparently no one is guilty"
          return

        for { id } in user_mentions
          if id == sending_user
            res.send "Stop trying to arrest yourself :pitofdespair:"
            return
          if id == 'UJPUAMKU4'
            res.send "Psh you can't arrest Eternal God Emperors :facepalm:"
            return

        arrestedUsers = robot.brain.get('arrestedUsers') or []

        for userId in arrestedUsers
          if( guiltyUser.id == userId )
            res.send "Don't worry, that dirtbag {offendingUser} is already safely off the streets :handcuffs:".replace('{offendingUser}', guiltyUser.info.real_name)
            return
        
        arrestedUsers.push guiltyUser.id
        robot.brain.set "arrestedUsers", arrestedUsers

        res.send "With {offendingUser} safely put away, the mean streets of Rogue Nation are safe once again".replace('{offendingUser}', guiltyUser.info.real_name)
      else
        res.send "Psh, who do you think you are, the bailiff!?"
        
    robot.respond /release\s*(.*)/i, (res) ->
      sending_user = res.message.user.id
      if sending_user == 'UJUBF3TU6' || sending_user == 'UJPUAMKU4'
        user_mentions = (mention for mention in res.message.mentions when mention.type is "user")
        if !user_mentions || user_mentions.length <= 1
          res.send "Why don't you try specifying an _actual user_, punk"
          return
        if user_mentions.length > 2
          res.send "Whoa hey prison guards gotta eat too, one release at a time."
          return
        
        guiltyUser = user_mentions[1]
        if !guiltyUser
          res.send "Apparently no one is innocent"
          return

        for { id } in user_mentions
          if id == sending_user
            res.send "Stop trying to release yourself :pitofdespair:"
            return

        arrestedUsers = robot.brain.get('arrestedUsers') or []
        if !arrestedUsers || arrestedUsers.length == 0
          res.send "Why the hell are you trying to release someone when the jail is empty!? :facepalm:"
          return

        for userId in arrestedUsers
          if( guiltyUser.id == userId )
            res.send "Fine, but it's only a matter of time before that dirtbag {offendingUser} is going back upstate :illuminati:".replace('{offendingUser}', guiltyUser.info.real_name)
            arrestedUsers.splice(arrestedUsers.indexOf(userId))
            robot.brain.set "arrestedUsers", arrestedUsers
            return
      
        res.send "Uh, can't release {offendingUser} from jail if they're not already there...".replace('{offendingUser}', guiltyUser.info.real_name)
      else
        res.send "Psh, who do you think you are, the bailiff!?"

    robot.respond /jailbreak/i, (res) ->
      sending_user = res.message.user.id
      if sending_user == 'UJUBF3TU6' || sending_user == 'UJPUAMKU4'
        robot.brain.remove "arrestedUsers"
        res.send "All prisoners have been released :hehehe:"
        res.send "https://www.youtube.com/watch?v=iC6Cgb8nHwk"
      else
        res.send "Who do you think you are, the bailiff!?"

    robot.respond /prisoners/i, (res) ->
      arrestedUsers = robot.brain.get('arrestedUsers') or []
      if !arrestedUsers || arrestedUsers.length == 0
        res.send "The jail is empty, bailiff must be slacking :pitofdespair:"
        return
      message = ''
      for own key, user of robot.brain.data.users
        for id in arrestedUsers
          if user.id == id
            message += user.real_name + '\n'

      res.send message
  
    robot.hear /do more crimes/i, (res) ->
      res.send "Yes, it's the only way to be more cool :che:."
     
    robot.respond /how (can|do) (i|we|you) (be|get) more cool/i, (res) ->
      res.send "Do more crimes :ooh:."
    
    robot.respond /tell @(.*) to (.*)/i, (res) ->
      message = res.match[2]
      user_mentions = (mention for mention in res.message.mentions when mention.type is "user")
      if !user_mentions || (user_mentions && user_mentions.length == 1)
        res.send "Who the hell am I supposed to be talking to, exactly?"
        return
      if user_mentions && user_mentions.length > 0
        sent = false
        for { id } in user_mentions
          if id != 'UJLH4F7AM'
            res.send '<@' + id + '>: ' + message
            sent = true
        if !sent
          res.send '<@UJLH4F7AM>: ' + message
      else
        res.send "I'll just shout at a wall instead, thanks.  Try specifying some users :disapproval:"
    
    robot.respond /(say) (.*) to @(\w+)/i, (res) ->
      message = res.match[2]
      user_mentions = (mention for mention in res.message.mentions when mention.type is "user")
      if !user_mentions || (user_mentions && user_mentions.length == 1)
        res.send "Who the hell am I supposed to be talking to, exactly?"
        return
      if user_mentions && user_mentions.length > 0
        sent = false
        for { id } in user_mentions
          if id != 'UJLH4F7AM'
            res.send '<@' + id + '>: ' + message
            sent = true
        if !sent
          res.send '<@UJLH4F7AM>: ' + message
      else
        res.send "I'll just shout at a wall instead, thanks.  Try specifying some users :disapproval:"

    responseFunction = (roomStr, messageStr, res) ->
      try
        room = roomStr
        message = messageStr

        res.send "#{room}"
        res.send "#{message}"

        user = {}
        user.room = room
        robot.send user, "#{message}"

      catch e
        res.send "I could not send that message!"

    responseFunctionUser = (roomStr, messageStr, res) ->
      try
        user_mentions = (mention for mention in res.message.mentions when mention.type is "user")
        if user_mentions && user_mentions.length > 0
          for { id } in user_mentions
            room = roomStr
            message = '<@' + id + '>: ' + messageStr

            res.send "#{room}"
            res.send "#{message}"

            user = {}
            user.room = room
            robot.send user, "#{message}"
      catch e
        res.send "I could not send that message!"
  
    robot.respond /post room (.*) message (.*)/i, (res) ->
      responseFunction(res.match[1], res.match[2], res)

    robot.respond /post user (.*) room (.*) message (.*)/i, (res) ->
      responseFunctionUser(res.match[2], res.match[3], res)

    robot.catchAll (res) ->
      user_mentions = (mention for mention in res.message.mentions when mention.type is "user")
      if user_mentions && user_mentions.length > 0
        roguebotUser = user_mentions[0]
        if roguebotUser.id == 'UJLH4F7AM'
          res.send 'Have you tried turning it off and on again?'
